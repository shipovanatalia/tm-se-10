package ru.shipova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.service.IDomainService;
import ru.shipova.tm.entity.Domain;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.entity.User;

import java.util.List;

public class DomainService implements IDomainService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public DomainService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getIUserService().load(domain.getUserList());
        serviceLocator.getIProjectService().load(domain.getProjectList());
        serviceLocator.getITaskService().load(domain.getTaskList());
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        @NotNull final String userId = domain.getUserId();
        @Nullable final List<User> userList = serviceLocator.getIUserService().getUserList();
        if (userList == null) return;
        @Nullable final List<Project> projectList = serviceLocator.getIProjectService().getListProject(userId);
        if (projectList == null) return;
        @Nullable final List<Task> taskList = serviceLocator.getITaskService().getListTask(userId);
        if (taskList == null) return;
        domain.setUserList(userList);
        domain.setProjectList(projectList);
        domain.setTaskList(taskList);
    }
}
