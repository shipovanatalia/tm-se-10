package ru.shipova.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.api.repository.IUserRepository;
import ru.shipova.tm.util.PasswordHashUtil;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public final class UserService implements IUserService {

    private @NotNull final IUserRepository userRepository;

    @Nullable
    private User currentUser;

    @NotNull
    public UserService(@NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
        currentUser = null;
    }

    @Nullable
    @Override
    public User authorize(@NotNull final String login, @NotNull final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean checkDataAccess(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) return false;
        if (password.isEmpty()) return false;

        @Nullable final User user = findByLogin(login);
        if (user == null) return false;

        @Nullable final String passwordHash = PasswordHashUtil.md5(password);
        if (passwordHash == null) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public RoleType getRoleType(@Nullable final String role){
        if (role == null || role.isEmpty()) return null;
        RoleType roleType = null;
        switch (role.toUpperCase()) {
            case "USER":
                roleType = RoleType.USER;
                break;
            case "ADMIN":
                roleType = RoleType.ADMIN;
                break;
        }
        return roleType;
    }

    @Override
    public void registryUser(@Nullable final String login, @Nullable final String password, @Nullable final String role) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (role == null || role.isEmpty()) return;

        @Nullable final String passwordHash = PasswordHashUtil.md5(password);
        @NotNull final String userId = UUID.randomUUID().toString();

        @Nullable final RoleType roleType = getRoleType(role);
        userRepository.persist(new User(userId, login, passwordHash, roleType));
    }

    @Override
    public void updateUser(@Nullable final String login, @Nullable final String role){
        if (role == null || role.isEmpty()) return;
        if (login == null || login.isEmpty()) return;

        @Nullable final RoleType roleType = getRoleType(role);
        userRepository.updateUser(login, roleType);
    }

    @Override
    public void setNewPassword(@Nullable final String login, @NotNull final String password) {
        if (password.isEmpty()) return;
        if (login == null || login.isEmpty()) return;

        @Nullable final String passwordHash = PasswordHashUtil.md5(password);
        userRepository.setNewPassword(login, passwordHash);
    }

    @Override
    @Nullable
    public List<User> getUserList() {
        return userRepository.findAll();
    }

    @Override
    public void load(@Nullable final List<User> userList) {
        if (userList == null) return;
        userRepository.load(userList);
    }
}
