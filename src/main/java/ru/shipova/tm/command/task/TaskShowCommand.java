package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.exception.ProjectDoesNotExistException;

public final class TaskShowCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-show";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks of project.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            System.out.println("ENTER NAME OF PROJECT:");
            @NotNull final ITaskService taskService = serviceLocator.getITaskService();
            @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
            System.out.println("PROJECT " + projectName + " CONTAINS TASKS:");
            try {
                for (@Nullable final String nameOfTask : taskService.showAllTasksOfProject(projectName)) {
                    System.out.println(nameOfTask);
                }
            } catch (ProjectDoesNotExistException e) {
                System.out.println("PROJECT DOES NOT EXISTS");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
