package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.entity.User;

import java.util.List;

public class TaskSearchCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-search";
    }

    @Override
    public String getDescription() {
        return "Search task by part of name.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            System.out.println("[TASK SEARCH]");
            @NotNull final ITaskService taskService = serviceLocator.getITaskService();
            @NotNull final String partOfData = serviceLocator.getTerminalService().nextLine();

            @NotNull final IUserService userService = serviceLocator.getIUserService();
            @Nullable final User currentUser = userService.getCurrentUser();
            @Nullable final String userId = currentUser != null ? currentUser.getId() : null;

            @Nullable final List<Task> taskList = taskService.search(userId, partOfData);
            if (taskList == null || taskList.isEmpty()) {
                System.out.println("TASKS ARE NOT FOUND");
                return;
            }

            int index = 1;
            for (@NotNull final Task task : taskList) {
                System.out.println(index++ + ". " + task.getName());
            }
            System.out.println();
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
