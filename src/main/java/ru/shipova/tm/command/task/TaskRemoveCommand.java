package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            System.out.println("[TASK REMOVE]");
            System.out.println("ENTER NAME OF TASK:");
            @NotNull final ITaskService taskService = serviceLocator.getITaskService();
            @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();

            @NotNull final IUserService userService = serviceLocator.getIUserService();
            @Nullable final User currentUser = userService.getCurrentUser();
            @Nullable final String userId = currentUser != null ? currentUser.getId() : null;

            taskService.remove(userId, projectName);
            System.out.println("[TASK " + projectName + " REMOVED]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
