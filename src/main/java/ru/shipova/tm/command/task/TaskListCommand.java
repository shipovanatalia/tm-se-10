package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.CommandCorruptException;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            System.out.println("[TASK LIST]");
            @NotNull final ITaskService taskService = serviceLocator.getITaskService();
            @NotNull final IUserService userService = serviceLocator.getIUserService();
            @NotNull final User currentUser = userService.getCurrentUser();
            @NotNull final String userId = currentUser.getId();

            sortTasks(taskService, userId);
        }
    }

    private void sortTasks(@NotNull final ITaskService taskService, @NotNull final String userId) {
        if (serviceLocator != null) {
            System.out.println("PLEASE CHOOSE TYPE OF SORT OF TASKS: ");
            System.out.println("1. DATE OF CREATE;");
            System.out.println("2. DATE OF BEGIN;");
            System.out.println("3. DATE OF END;");
            System.out.println("4. STATUS.");
            System.out.println("ENTER NAME OF SORT OR NO");
            @NotNull final String typeOfSort = serviceLocator.getTerminalService().nextLine();
            @Nullable final List<Task> listTask = taskService.getListTask(userId);
            if (listTask == null) {
                System.out.println("LIST OF TASKS DOES NOT EXISTS.");
                return;
            }
            @Nullable List<Task> sortedList = null;
            try {
                sortedList = taskService.sort(typeOfSort, listTask);
            } catch (CommandCorruptException e) {
                System.out.println("WRONG COMMAND.");
                System.out.println();
                sortTasks(taskService, userId);
            }

            int index = 1;
            if (sortedList == null) return;

            for (@Nullable final Task task : sortedList) {
                if (task == null) return;
                System.out.println(index++ + ". " + task.getName());
            }
            System.out.println();
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
