package ru.shipova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.command.AbstractCommand;

public final class ProjectCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            System.out.println("[PROJECT CREATE]");
            System.out.println("ENTER NAME:");
            @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
            @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
            @NotNull final String userId = serviceLocator.getIUserService().getCurrentUser().getId();
            projectService.create(userId, projectName);
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }


}
