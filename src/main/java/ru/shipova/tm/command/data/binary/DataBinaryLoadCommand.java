package ru.shipova.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Domain;
import ru.shipova.tm.serializer.BinarySerializer;

public class DataBinaryLoadCommand extends AbstractCommand {
    @Override
    @Nullable
    public String getName() {
        return "bin-load";
    }

    @Override
    @Nullable
    public String getDescription() {
        return "Load data from binary file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            System.out.println("[DATA BINARY LOAD]");
            @NotNull final ISerializer binarySerializer = new BinarySerializer();
            @Nullable final Domain domain = binarySerializer.deserialize();
            if (domain == null) return;
            serviceLocator.getIDomainService().load(domain);
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
