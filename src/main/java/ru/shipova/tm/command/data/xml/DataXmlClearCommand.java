package ru.shipova.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.DataConstant;

import java.io.File;
import java.io.PrintWriter;

public class DataXmlClearCommand extends AbstractCommand {
    @Override
    public @Nullable String getName() {
        return "xml-clear";
    }

    @Override
    public @Nullable String getDescription() {
        return "Clear XML-file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            System.out.println("[DATA XML CLEAR]");
            @NotNull final File file = new File(DataConstant.FILE_XML.displayName());
            @NotNull final PrintWriter writer = new PrintWriter(file);
            writer.print("");
            writer.flush();
            writer.close();
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
