package ru.shipova.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Domain;
import ru.shipova.tm.serializer.BinarySerializer;

public class DataBinarySaveCommand extends AbstractCommand {
    @Override
    @Nullable
    public String getName() {
        return "bin-save";
    }

    @Override
    @Nullable
    public String getDescription() {
        return "Save data to binary file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            System.out.println("[DATA BINARY SAVE]");
            @NotNull final String userId = serviceLocator.getIUserService().getCurrentUser().getId();
            @NotNull final Domain domain = new Domain();
            domain.setUserId(userId);
            serviceLocator.getIDomainService().export(domain);
            @NotNull final ISerializer binarySerializer = new BinarySerializer();
            binarySerializer.serialize(domain);
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
