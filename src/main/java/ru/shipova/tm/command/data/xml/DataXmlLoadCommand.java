package ru.shipova.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.TypeOfSerialization;
import ru.shipova.tm.entity.Domain;
import ru.shipova.tm.serializer.XmlFasterXmlSerializer;
import ru.shipova.tm.serializer.XmlJaxBSerializer;

public class DataXmlLoadCommand extends AbstractCommand {
    @Override
    public @Nullable String getName() {
        return "xml-load";
    }

    @Override
    public @Nullable String getDescription() {
        return "Load data from XML-file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            System.out.println("[DATA XML LOAD]");
            System.out.println("CHOOSE TYPE OF DESERIALIZATION:");
            System.out.println("1. FasterXml;");
            System.out.println("2. Jax-B.");
            @NotNull final String input = serviceLocator.getTerminalService().nextLine();
            @NotNull final String typeOfDeserialization = input.toUpperCase();
            @Nullable Domain domain = new Domain();
            if (typeOfDeserialization.equals("1") ||
                    typeOfDeserialization.equals(TypeOfSerialization.FASTER_XML.displayName())) {
                final ISerializer xmlFasterXmlSerializer = new XmlFasterXmlSerializer();
                domain = xmlFasterXmlSerializer.deserialize();
            }
            if (typeOfDeserialization.equals("2") ||
                    typeOfDeserialization.equals(TypeOfSerialization.JAX_B.displayName())) {
                @NotNull final ISerializer xmlJaxBSerializer = new XmlJaxBSerializer();
                domain = xmlJaxBSerializer.deserialize();
            }
            if (domain == null) return;
            serviceLocator.getIDomainService().load(domain);
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
