package ru.shipova.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.TypeOfSerialization;
import ru.shipova.tm.entity.Domain;
import ru.shipova.tm.serializer.JsonFasterXmlSerializer;
import ru.shipova.tm.serializer.JsonJaxBSerializer;

public class DataJsonLoadCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "json-load";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from JSON-file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            System.out.println("[DATA JSON LOAD]");
            System.out.println("CHOOSE TYPE OF DESERIALIZATION:");
            System.out.println("1. FasterXml;");
            System.out.println("2. Jax-B.");
            @NotNull final String input = serviceLocator.getTerminalService().nextLine();
            @NotNull final String typeOfDeserialization = input.toUpperCase();
            @Nullable Domain domain = new Domain();
            if (typeOfDeserialization.equals("1") ||
                    typeOfDeserialization.equals(TypeOfSerialization.FASTER_XML.displayName())) {
                @NotNull final ISerializer jsonFasterXmlSerializer = new JsonFasterXmlSerializer();
                domain = jsonFasterXmlSerializer.deserialize();
            }
            if (typeOfDeserialization.equals("2") ||
                    typeOfDeserialization.equals(TypeOfSerialization.JAX_B.displayName())) {
                @NotNull final ISerializer jsonJaxBSerializer = new JsonJaxBSerializer();
                domain = jsonJaxBSerializer.deserialize();
            }
            serviceLocator.getIDomainService().load(domain);
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
