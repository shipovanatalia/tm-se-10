package ru.shipova.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.DataConstant;

import java.io.File;
import java.io.PrintWriter;

public class DataBinaryClearCommand extends AbstractCommand {
    @Override
    @Nullable
    public String getName() {
        return "bin-clear";
    }

    @Override
    @Nullable
    public String getDescription() {
        return "Clear binary-file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            System.out.println("[DATA BINARY CLEAR]");
            @NotNull final File file = new File(DataConstant.FILE_BINARY.displayName());
            @NotNull final PrintWriter writer = new PrintWriter(file);
            writer.print("");
            writer.flush();
            writer.close();
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
