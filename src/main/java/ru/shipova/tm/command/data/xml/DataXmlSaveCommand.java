package ru.shipova.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.TypeOfSerialization;
import ru.shipova.tm.entity.Domain;
import ru.shipova.tm.serializer.XmlFasterXmlSerializer;
import ru.shipova.tm.serializer.XmlJaxBSerializer;

public class DataXmlSaveCommand extends AbstractCommand {

    @Override
    public @Nullable String getName() {
        return "xml-save";
    }

    @Override
    public @Nullable String getDescription() {
        return "Save data to XML.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            System.out.println("[DATA XML SAVE]");
            @NotNull final String userId = serviceLocator.getIUserService().getCurrentUser().getId();
            @NotNull final Domain domain = new Domain();
            domain.setUserId(userId);
            serviceLocator.getIDomainService().export(domain);
            System.out.println("CHOOSE TYPE OF SERIALIZATION:");
            System.out.println("1. FasterXml;");
            System.out.println("2. Jax-B.");
            @NotNull final String input = serviceLocator.getTerminalService().nextLine();
            @NotNull final String typeOfSerialization = input.toUpperCase();
            if (typeOfSerialization.equals("1") ||
                    typeOfSerialization.equals(TypeOfSerialization.FASTER_XML.displayName())) {
                final ISerializer xmlFasterXmlSerializer = new XmlFasterXmlSerializer();
                xmlFasterXmlSerializer.serialize(domain);
            }
            if (typeOfSerialization.equals("2") ||
                    typeOfSerialization.equals(TypeOfSerialization.JAX_B.displayName())) {
                @NotNull final ISerializer xmlJaxBSerializer = new XmlJaxBSerializer();
                xmlJaxBSerializer.serialize(domain);
            }
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
