package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sign out from Task Manager.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @NotNull final IUserService userService = serviceLocator.getIUserService();
            userService.setCurrentUser(null);
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
