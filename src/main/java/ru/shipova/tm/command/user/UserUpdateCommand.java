package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;

public final class UserUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update role type of user.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            System.out.println("[USER UPDATE]");
            @NotNull final IUserService userService = serviceLocator.getIUserService();

            System.out.println("ENTER LOGIN OF USER TO UPDATE:");
            @NotNull final String login = serviceLocator.getTerminalService().nextLine();

            System.out.println("ENTER NEW ROLE TYPE:");
            @NotNull final String newRoleType = serviceLocator.getTerminalService().nextLine();
            userService.updateUser(login, newRoleType);
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
