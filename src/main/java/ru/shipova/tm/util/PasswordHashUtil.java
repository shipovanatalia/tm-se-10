package ru.shipova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class PasswordHashUtil {
    @Nullable
    public static String md5(final @NotNull String password) {
        try {
            @NotNull MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            return new String(messageDigest.digest(password.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
