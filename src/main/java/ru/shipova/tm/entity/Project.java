package ru.shipova.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.Status;

import java.io.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project extends AbstractFieldOfUser implements Serializable {

    public static final long serialVersionUID = 1;

    @Nullable
    private String name;

    @Nullable
    private String userId;

    @Nullable
    private String description;

    @Nullable
    private Date dateOfBegin;

    @Nullable
    private Date dateOfEnd;

    @Nullable
    private Status status;

    public Project(@NotNull final String id, @Nullable final String userId, @Nullable final String name) {
        this.id = id;
        this.name = name;
        this.userId = userId;
    }
}
