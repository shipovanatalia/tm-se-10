package ru.shipova.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Domain implements Serializable {
    @NotNull private String userId = "";
    @Nullable private List<User> userList = new ArrayList<>();
    @Nullable private List<Project> projectList = new ArrayList<>();
    @Nullable private List<Task> taskList = new ArrayList<>();
}
