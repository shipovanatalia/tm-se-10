package ru.shipova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Domain;

public interface IDomainService {
    void load(@Nullable final Domain domain);
    void export(@NotNull final Domain domain);
}
