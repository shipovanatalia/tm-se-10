package ru.shipova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Domain;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface ISerializer {
    void serialize(@NotNull final Domain domain) throws IOException;
    @Nullable Domain deserialize() throws IOException, ClassNotFoundException;
}