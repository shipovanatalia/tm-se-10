package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;

import java.util.List;

public interface IUserRepository {
    @Nullable User findByLogin(@NotNull final String login);

    void persist(@NotNull final User user);

    void setNewPassword(@Nullable final String login,@Nullable final String passwordHash);

    void updateUser(@Nullable final String login, @Nullable final RoleType roleType);

    @Nullable List<User> findAll();

    void load(@NotNull final List<User> userList);
}
