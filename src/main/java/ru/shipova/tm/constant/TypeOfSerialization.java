package ru.shipova.tm.constant;

import org.jetbrains.annotations.NotNull;

public enum TypeOfSerialization {
    JAX_B("JAX-B"),
    FASTER_XML("FASTER XML");


    private @NotNull
    final String name;

    TypeOfSerialization(@NotNull final String name) {
        this.name = name;
    }

    @NotNull public String displayName() {
        return name;
    }
}
