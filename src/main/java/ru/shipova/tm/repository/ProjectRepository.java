package ru.shipova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.comparator.ComparatorByDateOfBegin;
import ru.shipova.tm.comparator.ComparatorByDateOfCreate;
import ru.shipova.tm.comparator.ComparatorByDateOfEnd;
import ru.shipova.tm.comparator.ComparatorByStatus;
import ru.shipova.tm.constant.Status;
import ru.shipova.tm.constant.TypeOfSort;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.exception.CommandCorruptException;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> projects = new ArrayList<>();

        for (@NotNull final Map.Entry<String, Project> entry : getMap().entrySet()) {
            if (userId.equals(entry.getValue().getUserId())) {
                projects.add(entry.getValue());
            }
        }
        return projects;
    }

    @NotNull
    @Override
    public String getProjectIdByName(@NotNull final String projectName) {
        for (@NotNull final Map.Entry<String, Project> entry : getMap().entrySet()) {
            if (projectName.equals(entry.getValue().getName())) {
                return entry.getKey();
            }
        }
        return "";
    }

    @Nullable
    @Override
    public List<Project> search(@Nullable final String userId, @NotNull final String partOfData) {
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (@Nullable final Map.Entry<String, Project> entry : getMap().entrySet()) {
            if (entry == null) return null;
            @Nullable final Project project = entry.getValue();
            if (project == null) return null;
            if (project.getName() == null) continue;
            if (project.getName().contains(partOfData)) {
                projectList.add(project);
            }
            if (project.getDescription() == null) continue;
            if (project.getDescription().contains(partOfData)) {
                projectList.add(project);
            }
        }
        return projectList;
    }

    @NotNull
    public List<Project> sort(@NotNull final String typeOfSort,
                              @NotNull final List<Project> projectList) throws CommandCorruptException {
        if (TypeOfSort.NO.displayName().equals(typeOfSort.toUpperCase())){
            return projectList;
        }
        if (TypeOfSort.STATUS.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Project> comparator = new ComparatorByStatus<Project>();
            projectList.sort(comparator);
            return projectList;
        }
        if (TypeOfSort.DATE_OF_CREATE.displayName().equals(typeOfSort.toUpperCase())){
            @NotNull final Comparator<Project> comparator = new ComparatorByDateOfCreate<>();
            projectList.sort(comparator);
            return projectList;
        }
        if (TypeOfSort.DATE_OF_BEGIN.displayName().equals(typeOfSort.toUpperCase())){
            @NotNull final Comparator<Project> comparator = new ComparatorByDateOfBegin<>();
            projectList.sort(comparator);
            return projectList;
        }
        if (TypeOfSort.DATE_OF_END.displayName().equals(typeOfSort.toUpperCase())){
            @NotNull final Comparator<Project> comparator = new ComparatorByDateOfEnd<>();
            projectList.sort(comparator);
            return projectList;
        }
        else throw new CommandCorruptException();
    }

    @Override
    public void persist(@NotNull final Project project) {
        if (isExist(project)) return;
        project.setStatus(Status.PLANNED);
        getMap().put(project.getId(), project);
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> projectsToRemove = findAllByUserId(userId);
        for (@Nullable final Project project : projectsToRemove) {
            if (project == null) return;
            for (Map.Entry<String, Project> entry : getMap().entrySet()) {
                if (project.getId().equals(entry.getKey())) {
                    remove(project);
                }
            }
        }
    }

    public void load(@NotNull final List<Project> projectList) {
        for (@Nullable final Project project : projectList) {
            if (project == null) return;
            @NotNull final String projectId = project.getId();
            getMap().put(projectId, project);
        }
    }

    @Override
    public void update(@NotNull final Project project) {
        for (@Nullable final Map.Entry<String, Project> entry : getMap().entrySet()) {
            if (entry == null) return;
            if (project.getId().equals(entry.getKey())) {
                entry.getValue().setName(project.getName());
                entry.getValue().setDescription(project.getDescription());
                entry.getValue().setDateOfCreate(project.getDateOfCreate());
                entry.getValue().setDateOfEnd(project.getDateOfEnd());
                entry.getValue().setStatus(project.getStatus());
            }
        }
    }
}
