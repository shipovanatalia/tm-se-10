package ru.shipova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.repository.ITaskRepository;
import ru.shipova.tm.comparator.ComparatorByDateOfBegin;
import ru.shipova.tm.comparator.ComparatorByDateOfCreate;
import ru.shipova.tm.comparator.ComparatorByDateOfEnd;
import ru.shipova.tm.comparator.ComparatorByStatus;
import ru.shipova.tm.constant.Status;
import ru.shipova.tm.constant.TypeOfSort;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.CommandCorruptException;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository() {
    }

    @NotNull
    public List<Task> sort(@NotNull final String typeOfSort,
                           @NotNull final List<Task> taskList) throws CommandCorruptException {
        if (TypeOfSort.NO.displayName().equals(typeOfSort.toUpperCase())) {
            return taskList;
        }
        if (TypeOfSort.STATUS.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Task> comparator = new ComparatorByStatus<Task>();
            taskList.sort(comparator);
            return taskList;
        }
        if (TypeOfSort.DATE_OF_CREATE.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Task> comparator = new ComparatorByDateOfCreate<>();
            taskList.sort(comparator);
            return taskList;
        }
        if (TypeOfSort.DATE_OF_BEGIN.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Task> comparator = new ComparatorByDateOfBegin<>();
            taskList.sort(comparator);
            return taskList;
        }
        if (TypeOfSort.DATE_OF_END.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Task> comparator = new ComparatorByDateOfEnd<>();
            taskList.sort(comparator);
            return taskList;
        } else throw new CommandCorruptException();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Task> tasks = new ArrayList<>();

        for (@NotNull final Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (userId.equals(entry.getValue().getUserId())) {
                tasks.add(entry.getValue());
            }
        }
        return tasks;
    }

    @Nullable
    @Override
    public String getTaskIdByName(@NotNull final String taskName) {
        for (@NotNull final Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (taskName.equals(entry.getValue().getName())) {
                return entry.getKey();
            }
        }
        return null;
    }

    @NotNull
    @Override
    public List<String> showAllTasksOfProject(@NotNull final String projectId) {
        @NotNull final List<String> listOfTasks = new ArrayList<>();

        for (@NotNull final Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (projectId.equals(entry.getValue().getProjectId())) {
                listOfTasks.add(entry.getValue().getName());
            }
        }
        return listOfTasks;
    }

    @Nullable
    @Override
    public List<Task> search(@Nullable final String userId, @NotNull final String partOfData) {
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (@Nullable final Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (entry == null) return null;
            @Nullable final Task task = entry.getValue();
            if (task == null) return null;
            if (task.getName() == null) continue;
            if (task.getName().contains(partOfData)) {
                taskList.add(task);
            }
            if (task.getDescription() == null) continue;
            if (task.getDescription().contains(partOfData)) {
                taskList.add(task);
            }
        }
        return taskList;
    }

    @Override
    public void persist(@NotNull final Task task) {
        if (isExist(task)) return;
        task.setStatus(Status.PLANNED);
        getMap().put(task.getId(), task);
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Task> tasksToRemove = findAllByUserId(userId);
        for (@Nullable final Task task : tasksToRemove) {
            if (task == null) return;
            for (@Nullable final Map.Entry<String, Task> entry : getMap().entrySet()) {
                if (entry == null) return;
                if (task.getId().equals(entry.getKey())) {
                    remove(task);
                }
            }
        }
    }

    @Override
    public void update(@NotNull final Task task) {
        for (@NotNull final Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (task.getId().equals(entry.getKey())) {
                entry.getValue().setName(task.getName());
                entry.getValue().setDescription(task.getDescription());
                entry.getValue().setProjectId(task.getProjectId());
                entry.getValue().setDateOfBegin(task.getDateOfBegin());
                entry.getValue().setDateOfEnd(task.getDateOfEnd());
                entry.getValue().setStatus(task.getStatus());
            }
        }
    }

    @Override
    public void removeAllTasksOfProject(@NotNull final String projectId) {
        getMap().entrySet().removeIf(entry -> projectId.equals(entry.getValue().getProjectId()));
    }

    @Override
    public void load(@NotNull final List<Task> taskList) {
        for (@Nullable final Task task : taskList) {
            if (task == null) return;
            @NotNull final String taskId = task.getId();
            getMap().put(taskId, task);
        }
    }
}
