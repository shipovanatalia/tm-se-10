package ru.shipova.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.AbstractEntity;
import ru.shipova.tm.entity.AbstractFieldOfUser;

import java.util.Comparator;
import java.util.Date;

public class ComparatorByDateOfBegin<T extends AbstractFieldOfUser> implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        @Nullable final Date firstDate = ((AbstractFieldOfUser) o1).getDateOfBegin();
        @Nullable final Date secondDate = ((AbstractFieldOfUser) o2).getDateOfBegin();
        if (firstDate == null) return -1;
        if (secondDate == null) return 1;
        return (firstDate.compareTo(secondDate));
    }
}
