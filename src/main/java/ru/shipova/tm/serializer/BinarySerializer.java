package ru.shipova.tm.serializer;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.constant.DataConstant;
import ru.shipova.tm.entity.Domain;

import java.io.*;

public class BinarySerializer implements ISerializer {
    @Override
    public void serialize(@NotNull final Domain domain) throws IOException {
        @NotNull final File file = new File(DataConstant.FILE_BINARY.displayName());
        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
             @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(domain);
        }
    }

    @Override
    @Nullable
    public Domain deserialize() throws IOException, ClassNotFoundException {
        @NotNull final File file = new File(DataConstant.FILE_BINARY.displayName());
        try(@NotNull final FileInputStream fileInputStream = new FileInputStream(file);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            @Nullable final Domain domain = (Domain) objectInputStream.readObject();
            return domain;
        }
    }
}
