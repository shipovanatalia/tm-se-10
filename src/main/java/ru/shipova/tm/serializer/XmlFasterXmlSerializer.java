package ru.shipova.tm.serializer;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.constant.DataConstant;
import ru.shipova.tm.entity.Domain;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class XmlFasterXmlSerializer implements ISerializer {
    @NotNull
    private final XmlMapper xmlMapper = new XmlMapper();

    @Override
    public void serialize(@NotNull final Domain domain) throws IOException {
        xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_1_1, true);
        @NotNull final ObjectWriter objectWriter = xmlMapper.writerWithDefaultPrettyPrinter();
        @NotNull final String xml = objectWriter.writeValueAsString(domain);
        @NotNull final byte[] data = xml.getBytes(StandardCharsets.UTF_8);
        @NotNull final File file = new File(DataConstant.FILE_XML.displayName());
        Files.write(file.toPath(), data);
    }

    @Override
    @Nullable
    public Domain deserialize() throws IOException {
        @NotNull final File file = new File(DataConstant.FILE_XML.displayName());
        @Nullable final Domain domain = xmlMapper.readValue(file, Domain.class);
        return domain;
    }
}
